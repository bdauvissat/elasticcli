package fr.dauvissat;

import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class GenerateEnv {

    @Inject
    Template env;


    public TemplateInstance loadEnv(String elasticVersion, String elasticPwd, int elasticPort, String kibanaPwd,
                                    int kibanaPort, String clusterName, String license) {
        Map<String, Object> input = new HashMap<>();
        input.put("version", elasticVersion);
        input.put("elasticpwd", elasticPwd);
        input.put("esPort", elasticPort);
        input.put("kibanapwd", kibanaPwd);
        input.put("kibanaPort", kibanaPort);
        input.put("clusterName", clusterName);
        input.put("license", license);

        var template = env.data(input);

        try {
            Tools.writeToFile(".env", template.render());
        } catch (IOException e) {
            throw new RuntimeException("Problem on file creation");
        }

        return env.data(input);
    }

}
