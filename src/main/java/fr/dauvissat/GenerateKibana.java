package fr.dauvissat;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class GenerateKibana {

    @Inject
    Template kibana;

    public String kibanaPart() {

        return kibana.render();

    }

}
