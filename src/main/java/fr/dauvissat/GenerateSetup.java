package fr.dauvissat;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class GenerateSetup {

    @Inject
    Template instance;

    @Inject
    Template setup;

    public String get(List<String> nodes) {

        if (nodes == null) {
            return "";
        }

        String firstNode = nodes.get(0);
        String instances = getInstances(nodes);

        Map<String, Object> inputs = new HashMap<>();
        inputs.put("firstNode", firstNode);
        inputs.put("instances", instances);
        var tpl = setup.data(inputs);

        return tpl.render().replace("\\\\", "\\");

    }

    private String getInstances(List<String> nodes)
    {
        StringBuilder data = new StringBuilder();

        for (String node: nodes) {
            var tpl = instance.data("node", node);
            data.append(tpl.render());
        }
        return data.toString();
    }

}
