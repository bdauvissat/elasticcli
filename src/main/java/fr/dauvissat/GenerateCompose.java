package fr.dauvissat;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class GenerateCompose {

    @Inject
    Template compose;

    @Inject
    GenerateKibana generateKibana;

    @Inject
    GenerateCluster generateCluster;

    @Inject
    GenerateSetup generateSetup;

    public void createComposeFile(String fileName, int nbNodes, boolean kibana) {

        Map<String, Object> values = new HashMap<>();

        if (nbNodes > 0) {
            String clusterPart = generateCluster.clusterPart(nbNodes);
            values.put("setup", generateSetup.get(generateCluster.getNodes()));
            values.put("cluster", clusterPart);
            values.put("volumes", generateCluster.getNodes());
        }

        if (kibana) {
            String kibanaPart = generateKibana.kibanaPart();
            values.put("kibana", kibanaPart);

        }

        var tpl = compose.data(values);

        try {
            Tools.writeToFile(fileName, tpl.render());
        } catch (IOException e) {
            throw new RuntimeException("Problem on file creation");
        }

    }

}
