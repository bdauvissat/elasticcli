package fr.dauvissat;

import jakarta.inject.Inject;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

@Command(name = "compose", mixinStandardHelpOptions = true)
public class ComposeCommand implements Runnable {


    @Parameters(index = "0", paramLabel = "Nodes", description = "Number of nodes. 0 is possible")
    Integer nbNodes;

    @Parameters(index = "1", paramLabel = "Kibana", description = "Generate Kibana part")
    Boolean kibana;
    @Parameters(index = "2", paramLabel = "Compose file name", defaultValue = "docker-compose.yml")
    String fileName;

    @CommandLine.Option(names = {"-s", "--stack-version"}, paramLabel = "Stack version", required = true)
    String stackVersion;

    @CommandLine.Option(names = {"--elastic-pwd"}, paramLabel = "Pasword for user elastic", defaultValue = "elasticpwd")
    String elasticPwd;
    @CommandLine.Option(names = {"--elastic-port"}, paramLabel = "Elastic port", defaultValue = "9200")
    int elasticPort;
    @CommandLine.Option(names = {"--kibana-password"}, paramLabel = "Password for user kibana_system", defaultValue =
            "kibanapwd")
    String kibanaPwd;
    @CommandLine.Option(names = {"--kibana-port"}, paramLabel = "Kibana port", defaultValue = "5601")
    int kibanaPort;
    @CommandLine.Option(names = {"--cluster-name"}, paramLabel = "Stack version", defaultValue = "cluster01")
    String clusterName;

    @CommandLine.Option(names = {"--license"}, paramLabel = "License level", defaultValue = "basic")
    String license;

    @Inject
    GenerateCompose generateCompose;

    @Inject
    GenerateEnv generateEnv;

    @Override
    public void run() {

        if (nbNodes == null) {
            String s = System.console().readLine("Specify the number of nodes. O is possible");
            nbNodes = Integer.valueOf(s);
        }

        generateCompose.createComposeFile(fileName, nbNodes, kibana);
        generateEnv.loadEnv(stackVersion, elasticPwd, elasticPort, kibanaPwd, kibanaPort, clusterName, license);

    }

}
