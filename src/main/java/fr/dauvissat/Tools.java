package fr.dauvissat;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Tools {

    public static void writeToFile(String fileName, String data) throws IOException {

        try (Writer fileWriter = new FileWriter(fileName)) {
            fileWriter.write(data);
        }

    }

}
