package fr.dauvissat;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class GenerateCluster {

    @Inject
    Template cluster;

    private List<String> nodes = new ArrayList<>();

    public String clusterPart(int nbNodes) {

        nodes = nodeList(nbNodes);

        StringBuilder sb = new StringBuilder();
        String prevStep = "setup";

        for (String node: nodes) {

            Map<String, Object> values = new HashMap<>();
            values.put("isFirstNode", node.equals(nodes.get(0)));
            values.put("prevStep", prevStep);
            values.put("nodeName", node);
            values.put("nodesList", String.join(",", nodes));
            if (nbNodes > 1) {
                values.put("seedList", nodes.stream().filter(s -> !s.equals(node)).collect(Collectors.joining(",")));
            }

            var ti = cluster.data(values);
            sb.append(ti.render());
            prevStep = node;

        }

        return sb.toString();

    }

    private List<String> nodeNumbers(int nbNodes) {

        if (nbNodes <= 0) {
            return Collections.emptyList();
        }

        List<String> nodes = new ArrayList<>(nbNodes);

        for (int i = 1; i <= nbNodes; i++) {

            nodes.add(String.format("%02d", i));

        }

        return nodes;

    }

    private List<String> nodeList(int nbNodes) {

        List<String> nodeNumbers = nodeNumbers(nbNodes);

        return nodeNumbers.stream().map( s -> "es" + s).collect(Collectors.toList());

    }

    public List<String> getNodes() {
        return nodes;
    }
}
