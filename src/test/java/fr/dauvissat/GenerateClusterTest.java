package fr.dauvissat;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class GenerateClusterTest {

    @Inject
    GenerateCluster generateCluster;

    @Test
    void testOneNode() {

        String result = generateCluster.clusterPart(1);
        assertTrue(result.contains("es01"));
        assertFalse(result.contains("es02"));
    }

    @Test
    void testTwoNodes() {

        String result = generateCluster.clusterPart(2);
        assertTrue(result.contains("es01"));
        assertTrue(result.contains("es02"));
    }
}
