package fr.dauvissat;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class GenerateComposeTest {

    @Inject
    GenerateCompose generateCompose;

    @Test
    void testComposeKibanaOnly() {

        String fileName = "compose1.txt";
        generateCompose.createComposeFile(fileName, 0, true);
        Path path = Path.of(fileName);
        boolean exists = Files.exists(path);
        assertTrue(exists);
    }

    @Test
    void testComposeClusterOnlyOneNode() {

        String fileName = "compose2.txt";
        generateCompose.createComposeFile(fileName, 1, false);
        Path path = Path.of(fileName);
        boolean exists = Files.exists(path);
        assertTrue(exists);
    }

    @Test
    void testComposeClusterOnlyTwoNodes() {

        String fileName = "compose3.txt";
        generateCompose.createComposeFile(fileName, 2, false);
        Path path = Path.of(fileName);
        boolean exists = Files.exists(path);
        assertTrue(exists);
    }

    @Test
    void testComposeKibanaOneNode() {

        String fileName = "compose4.txt";
        generateCompose.createComposeFile(fileName, 1, true);
        Path path = Path.of(fileName);
        boolean exists = Files.exists(path);
        assertTrue(exists);
    }

    @Test
    void testComposeKibanaTwoNodes() {

        String fileName = "compose5.txt";
        generateCompose.createComposeFile(fileName, 2, true);
        Path path = Path.of(fileName);
        boolean exists = Files.exists(path);
        assertTrue(exists);
    }

}
