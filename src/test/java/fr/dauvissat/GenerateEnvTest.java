package fr.dauvissat;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class GenerateEnvTest {

    @Inject
    GenerateEnv env;

    @Test
    void loadEnv() {

        var retour = env.loadEnv("8.11.1", "elasticpwd", 9200, "kibanapwd", 5601, "test", "basic");
        assertNotNull(retour);

    }
}
