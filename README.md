# elasticcli

The goal of this project is to provide a docker compose file generator for an Elasticsearch cluster with all self-signed certificates.

You can specify the stack version, the number of nodes.

type --help in the command to have all the options

The options are :
- --stack-version, -s : The Elastic stack version you wish to use. It's mandatory.
- --elastic-pwd : The password for the "elastic" user. Default is "elasticpwd".
- --elastic-port : The port for Elasticsearch. Default is 9200.
- --kibana-password : The password for the "Kibana_system" user. Default is "kibanapwd".
- --kibana-port : The port for Kibana. Default is 5601.
- --cluster-name : The name of the cluster. Default is "cluster01".
- --license : The license level you wish to use. Default is "basic"

The parameters are :
- Number of nodes. Integer. It's mandatory. 0 is possible if you want to only generate a docker compose file about Kibana.
- Do you want Kibana in the compose file ? Boolean. Default is true.
- The compose filename you wish. String. The default is "docker-compose.yml"

## More to come
- Nodes roles
- Maybe cross-cluster operations
- Any other suggestions

## Stack used to build this CLI :
- Quarkus
- Picocli
- Qute

## Packaging and running the application

The application can be packaged using an _über-jar_ :

```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Dnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Dnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/elasticcli-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.
